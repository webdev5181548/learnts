// default
// enums will initialize the first value to 0 and add 1 to each additional value
enum CardinalDirections {
    North,
    East,
    South,
    West
  }
  let currentDirection = CardinalDirections.North;
  // logs 0
  console.log(currentDirection);
  // currentDirection = 'North';