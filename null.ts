// type
//let value: string | undefined | null = null;
//value = 'hello';
//value = undefined;

// optional chaining
interface House {
    sqft: number;
    yard?: {
      sqft: number;
    };
  }
  function printYardSize(house: House) {
    const yardSize = house.yard?.sqft;
    if (yardSize === undefined) {
      console.log('No yard');
    } else {
      console.log(`Yard is ${yardSize} sqft`);
    }
  }
  
  let home: House = {
    sqft: 500
  };
  
  printYardSize(home); // Prints 'No yard'

// nullish coalescence
  function printMileage(mileage: number | null | undefined) {
    console.log(`Mileage: ${mileage ?? 'Not Available'}`);
  }
  
  printMileage(null); // Prints 'Mileage: Not Available'
  printMileage(0); // Prints 'Mileage: 0'

// null assertion
//function getValue(): string | undefined {
//    return 'hello';
//  }
//  let value = getValue();
//  console.log('value length: ' + value!.length);

// array bounds handling
//let array: number[] = [1, 2, 3];
//let value = array[0]; // with `noUncheckedIndexedAccess` this has the type `number | undefined`