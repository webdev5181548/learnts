// interfaces
// คล้าย aliases แต่ใช้ได้แค่กับ object types
interface Rectangle {
    height: number,
    width: number
  }
  
  const rectangle: Rectangle = {
    height: 20,
    width: 10
  };